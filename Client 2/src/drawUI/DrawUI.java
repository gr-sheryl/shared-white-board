package drawUI;

import java.net.Socket;
import java.awt.*;
import javax.swing.*;



public class DrawUI extends JFrame{
	
	public Socket socket;
	public ClientControl control;
	public Graphics2D g;
	public DrawListener dl;
    public static TextField tf;
    
	public DrawUI() {
		try {
			socket = new Socket("192.168.43.89",9090);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}
	
    // 界面初始化方法
    public void initFrame() {
        setTitle("画图");//窗体名称
        setSize(700, 650);//窗体大小
        setDefaultCloseOperation(3);
        setLocationRelativeTo(null);//窗体居中
        //流式布局左对齐
        FlowLayout layout = new FlowLayout(FlowLayout.LEFT);
        setLayout(layout);//窗体使用流式布局管理器
        this.setResizable(true);//窗体大小可变

        //使用数组保存按钮名
        String buttonName[] = { "直线", "圆形", "曲线", "矩形","正方形","橡皮擦"};
        //用于保存图形按钮，使用网格布局
        JPanel jp1=new JPanel(new GridLayout(buttonName.length, 1,10,10));
        jp1.setPreferredSize(new Dimension(100, 200));

        dl=new DrawListener();
        //循环为按钮面板添加按钮
        for (int i = 0; i < buttonName.length; i++) {
            JButton jbutton = new JButton(buttonName[i]);
            jbutton.addActionListener(dl);//为按钮添加监听
            jp1.add(jbutton);
        }

        JPanel jp2=new JPanel();//画布面板
        jp2.setPreferredSize(new Dimension(550, 550));
        jp2.setBackground(Color.WHITE);


        // 定义Color数组，用来存储按钮上要显示的颜色信息
        Color[] colorArray = { Color.BLUE, Color.GREEN, Color.RED, Color.YELLOW};
        //用于保存颜色按钮的面板
        JPanel jp3=new JPanel(new GridLayout(colorArray.length,1,3,3));
        // 循环遍历colorArray数组，根据数组中的元素来实例化按钮对象
        for (int i = 0; i < colorArray.length; i++) {
            JButton button = new JButton();
            button.setBackground(colorArray[i]);
            button.setPreferredSize(new Dimension(30, 30));
            button.addActionListener(dl);//为按钮添加监听
            jp3.add(button);
        }
        
        
        //添加文本输入框
        JPanel jp4 = new JPanel();
        tf = new TextField(30);
        tf.setBackground(Color.WHITE);
        tf.addActionListener(dl);
        JButton btu = new JButton("发送文字");
        btu.addActionListener(dl);

        jp4.add(btu);
        jp4.add(tf);

        
        //将面板添加到主窗体
        this.add(jp1);
        this.add(jp2);
        this.add(jp3);
        this.add(jp4);
        //设置窗体的组件可见，如果为FALSE就看不到任何组件
        setVisible(true);
        //获取画笔对象
        g=(Graphics2D)jp2.getGraphics();
        g.setColor(Color.RED);
        //控制器初始化
      	control = new ClientControl(g,socket);
        //实例化DrawListener对象
        dl.setG(g);
        dl.setControl(control);
        //为面板添加鼠标监听，用于绘制图形
        jp2.addMouseListener(dl);
        jp2.addMouseMotionListener(dl);
        //控制器接收数据并处理数据
      	control.receiveData();
    }
	
}
