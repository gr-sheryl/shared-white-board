package drawUI;
 
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import javax.swing.JButton;

 
 
public class DrawListener implements MouseListener,ActionListener,MouseMotionListener  {
	
    private Color color;//颜色属性
    private Graphics2D g;//画笔属性
    private String str;//保存按钮上的字符串，区分不同的按钮
    private int x1,y1,x2,y2;//(x1,y1),(x2,y2)分别为鼠标的按下和释放时的坐标
    private String text;	//发送的文字
    
	public ClientControl control;	//用control.g获取画笔属性

	//构造函数接受控制器对象
	public void setControl(ClientControl control) {
		this.control=control;
	}
	
	//鼠标拖动的方法
    public void mouseDragged(MouseEvent e) {
        //画曲线的方法
        if ("曲线".equals(str)) {
        	System.out.println("曲线");
            int x, y;
            x = e.getX();
            y = e.getY();
            g.setStroke(new BasicStroke(1));
            g.drawLine(x, y, x1, y1);
            control.sendData(3, x1, y1, x, y, 1,"");
            x1 = x;
            y1 = y;
        }else if("橡皮擦".equals(str)) {
        	System.out.println("橡皮擦");
        	int x, y;
            x = e.getX();
            y = e.getY();
            g.drawLine(x, y, x1, y1);
            //设置线宽为10.0
            g.setStroke(new BasicStroke(15));
            g.setPaint(Color.WHITE);
            control.sendData(6, x1, y1, x, y, 15,"");
            x1 = x;
            y1 = y;
        }
    }

    @Override
    //鼠标移动方法
    public void mouseMoved(MouseEvent e) {

    }

    @Override
    //鼠标单击方法
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    //鼠标按下方法
    public void mousePressed(MouseEvent e) {
        x1=e.getX();//获取按下时鼠标的x坐标
        y1=e.getY();//获取按下时鼠标的y坐标
        if("发送文字".equals(str)) {
        	System.out.println("发送文字");
        	Font font = new Font("Arial", Font.BOLD,16);
        	g.setFont(font);
        	g.drawString(text, x1, y1);
        	control.sendData(7, x1, y1, 0, 0, 1,text);
        }
    }

    @Override
    //鼠标释放方法
    public void mouseReleased(MouseEvent e) {
        x2=e.getX();//获取释放时鼠标的x坐标
        y2=e.getY();//获取释放时鼠标的y坐标
        //画直线的方法
        if ("直线".equals(str)) {
        	System.out.println("直线");
            g.setStroke(new BasicStroke(1));
            g.drawLine(x1, y1, x2, y2);
            control.sendData(1, x1, y1, x2, y2, 1,"");
        }else if("圆形".equals(str)) {
        	System.out.println("圆形");
            g.setStroke(new BasicStroke(1));
        	g.drawOval(Math.min(x1, x2),Math.min(y1, y2),Math.abs(x2-x1),Math.abs(x2-x1));
            control.sendData(2, Math.min(x1, x2),Math.min(y1, y2),Math.abs(x2-x1),Math.abs(x2-x1), 1,"");
        }else if("矩形".equals(str)) {
        	System.out.println("矩形");
            g.setStroke(new BasicStroke(1));
        	g.drawRect(Math.min(x1, x2),Math.min(y1, y2),Math.abs(x2-x1),Math.abs(y2-y1));
        	control.sendData(4,Math.min(x1, x2),Math.min(y1, y2),Math.abs(x2-x1),Math.abs(y2-y1),1,"");
        }else if("正方形".equals(str)) {
        	System.out.println("正方形");
            g.setStroke(new BasicStroke(1));
        	g.drawRect(Math.min(x1, x2),Math.min(y1, y2),Math.abs(x2-x1),Math.abs(x2-x1));
        	control.sendData(5,Math.min(x1, x2),Math.min(y1, y2),Math.abs(x2-x1),Math.abs(x2-x1),1,"");
        }
    }

    @Override
    //鼠标进入方法
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    //鼠标退出方法
    public void mouseExited(MouseEvent e) {

    }

    @Override
    //处理按钮上的鼠标点击动作
    public void actionPerformed(ActionEvent e) {
    	text = DrawUI.tf.getText();
        //判断是颜色按钮还是图形按钮
        if ("".equals(e.getActionCommand())) {
            JButton jb = (JButton) e.getSource();
            color = jb.getBackground();
            g.setColor(color);//改变画笔的颜色

        } else {
            str = e.getActionCommand();
        }
    }

	public void setG(Graphics2D g) {
		// TODO Auto-generated method stub
        this.g = g;
	}
}