package Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class MyServer {
	
	public ServerSocket server;
	public static ArrayList<ServerThread> list = new ArrayList<ServerThread>();
	
	//启动服务器
	public void startServer() throws IOException {
		try {
			server = new ServerSocket(9090);
			System.out.println("服务器已经成功启动...");
			
			//服务器一直监听端口
			while(true) {
				//接收客户端连接
				Socket socket = server.accept();
				System.out.println("客户端:"+socket.getInetAddress()+"连接进来了...");
				
				//分配线程处理客户端请求队列
				ServerThread st = new ServerThread(socket);
				st.start();
				list.add(st);
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} 
		
	}
}
