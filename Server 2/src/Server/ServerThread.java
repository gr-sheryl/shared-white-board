package Server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketException;

public class ServerThread extends Thread{
	
	public Socket socket;
	public DataInputStream din;
	public DataOutputStream dout;


	public ServerThread(Socket socket) {
		this.socket = socket;
	}

	public void run() {
		try {
			//获得输入输出流
			InputStream ins = socket.getInputStream();
			OutputStream outs = socket.getOutputStream();
			//将字节流包装成数据流
			din = new DataInputStream(ins);
			dout = new DataOutputStream(outs);
			
			while(true) {
				//读取客户端发送的数据
				int value = din.read();
				//服务器向其他客户端发送消息
				for (int i = 0; i < MyServer.list.size(); i++) {
					ServerThread st = MyServer.list.get(i);
					if(st != this) {
						st.dout.write(value);
						st.dout.flush();
					}
				}
				
			}
		} catch (SocketException e) {
			try {
				//客户端退出，从请求队列中删除请求，并释放连接
				MyServer.list.remove(this);
				this.socket.close();
				System.out.println("客户端"+socket.getInetAddress()+"退出...");
				
			} catch (IOException e2) {
				e2.printStackTrace();
			}
				
		} catch (Exception e){
			e.printStackTrace();

		}
	}


}
